package com.myretail;

import com.myretail.Models.Product;
import com.myretail.Models.ProductCost;
import com.myretail.Repositories.ProductCostRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("/test.properties")
public class HarvesterApplicationTests {

	private final int TEST_PRODUCT_ID = 13860428;
	private final double TEST_PRODUCT_COST_VALUE = 4.99;
	private final String TEST_PRODUCT_COST_CURRENCY = "USD";
	private final String TEST_PRODUCT_TITLE = "The Big Lebowski (Blu-ray)";

	@Autowired
	private ProductCostRepository productCostRepository;

	@Autowired
	private TestRestTemplate restTemplate;

	private Product testProduct;

	@Before
	public void setUp(){
		this.testProduct = new Product();
		this.testProduct.setProductId(TEST_PRODUCT_ID);
		this.testProduct.setName(TEST_PRODUCT_TITLE);
		ProductCost productCost = new ProductCost();
		productCost.setProductId(TEST_PRODUCT_ID);
		productCost.setValue(TEST_PRODUCT_COST_VALUE);
		productCost.setCurrencyCode(TEST_PRODUCT_COST_CURRENCY);
		this.productCostRepository.save(productCost);

		ProductCost anotherProductCost = new ProductCost();
		anotherProductCost.setValue(TEST_PRODUCT_COST_VALUE);
		anotherProductCost.setCurrencyCode(TEST_PRODUCT_COST_CURRENCY);
		this.testProduct.setProductCost(anotherProductCost);
	}

	@After
	public void tearDown(){
		this.productCostRepository.deleteAll();;
	}

	@Test
	public void getProductById() {
		ResponseEntity<Product> response = this.restTemplate.getForEntity("/products/{id}",
				Product.class, this.testProduct.getProductId());
		Product product = response.getBody();
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(product).isEqualToComparingFieldByFieldRecursively(this.testProduct);
	}

	@Test
	public void putProductById() {
		ResponseEntity<Product> response = this.restTemplate.getForEntity("/products/{id}",
				Product.class, this.testProduct.getProductId());
		Product product = response.getBody();
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
		product.getProductCost().setValue(9.99);
		HttpEntity<?> httpEntity = new HttpEntity<>(product, headers);
		this.restTemplate.put("/products/{id}", httpEntity, this.testProduct.getProductId());
		ResponseEntity<Product> updatedResponse = this.restTemplate.getForEntity("/products/{id}",
				Product.class, this.testProduct.getProductId());
		Product updatedProduct = updatedResponse.getBody();
		assertThat(updatedProduct.getProductCost().getValue()).isEqualTo(9.99);
	}

}
