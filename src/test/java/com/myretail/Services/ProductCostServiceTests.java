package com.myretail.Services;

import com.myretail.Models.Product;
import com.myretail.Models.ProductCost;
import com.myretail.Repositories.ProductCostRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by michael.burris on 9/25/16.
 */
@RunWith(SpringRunner.class)
public class ProductCostServiceTests {

    private final int TEST_PRODUCT_ID = 13860428;

    @Mock
    private ProductCostRepository productCostRepository;

    @Mock
    private ProductCost productCost;

    private Product product;

    @Before
    public void setUp() {
        product = new Product();
        product.setProductId(TEST_PRODUCT_ID);
    }

    @Test
    public void testGetProductCost() {
        when(this.productCostRepository.findByProductId(product.getProductId())).thenReturn(productCost);
        ProductCostService productCostService = new ProductCostServiceImpl(this.productCostRepository);
        ProductCost retrievedProductCost = productCostService.getProductCost(product.getProductId());
        assertThat(retrievedProductCost).isEqualTo(this.productCost);
    }

    @Test
    public void testUpdateProductCostValue() {
        when(this.productCostRepository.findByProductId(this.product.getProductId())).thenReturn(productCost);
        ProductCostService productCostService = new ProductCostServiceImpl(this.productCostRepository);
        productCostService.updateProductCostValue(this.product.getProductId(), 9.99);
        verify(this.productCost, times(1)).setValue(9.99);
        verify(this.productCostRepository, times(1)).save(this.productCost);
    }
}

