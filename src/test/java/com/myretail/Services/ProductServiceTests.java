package com.myretail.Services;

import com.myretail.Models.Product;
import com.myretail.Models.ProductCost;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by michael.burris on 9/25/16.
 */
@RunWith(SpringRunner.class)
public class ProductServiceTests {

    private final int TEST_PRODUCT_ID = 13860428;
    private final double TEST_PRODUCT_COST_VALUE = 4.99;
    private final String TEST_PRODUCT_COST_CURRENCY = "USD";
    private final String TEST_PRODUCT_TITLE = "The Big Lebowski (Blu-ray)";

    @Mock
    private ProductCostService productCostService;

    @Mock
    private ProductCompositeService productCompositeService;

    private Product product;
    private ProductCost productCost;

    @Before
    public void setUp() {
        product = new Product();
        product.setName(TEST_PRODUCT_TITLE);
        product.setProductId(TEST_PRODUCT_ID);
        productCost = new ProductCost();
        productCost.setProductId(TEST_PRODUCT_ID);
        productCost.setValue(TEST_PRODUCT_COST_VALUE);
        productCost.setCurrencyCode(TEST_PRODUCT_COST_CURRENCY);
        product.setProductCost(productCost);
    }

    @Test
    public void testFindProductById() throws Exception {
        given(this.productCompositeService.getOnlineDescription(product.getProductId())).willReturn(TEST_PRODUCT_TITLE);
        given(this.productCostService.getProductCost(product.getProductId())).willReturn(this.productCost);
        ProductService productService = new ProductServiceImpl(productCostService, productCompositeService);
        assertThat(productService.findProductById(product.getProductId()))
                .isEqualToComparingFieldByFieldRecursively(product);
    }

    @Test
    public void editProductPrice() {
        ProductService productService = new ProductServiceImpl(productCostService, productCompositeService);
        productService.updateProductCost(product.getProductId(), 9.99);
        verify(this.productCostService, times(1)).updateProductCostValue(product.getProductId(), 9.99);
    }
}
