package com.myretail.Services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.client.ExpectedCount.min;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

/**
 * Created by michael.burris on 9/25/16.
 */
@RunWith(SpringRunner.class)
@RestClientTest(ProductCompositeServiceImpl.class)
@TestPropertySource("/test.properties")
public class ProductCompositeServiceTests {

    @Value("${api.host}")
    private String PRODUCT_COMPOSITE_HOST;

    @Value("${api.products_path}")
    private String PRODUCTS_PATH;

    @Value("${api.version}")
    private String PRODUCT_COMPOSITE_API_VERSION;

    @Value("${api.fields.param}")
    private String FIELDS_PARAM;

    @Value("${api.fields.value}")
    private String FIELDS_VALUE;

    @Value("${api.id_type.param}")
    private String ID_TYPE_PARAM;

    @Value("${api.id_type.value}")
    private String ID_TYPE_VALUE;

    @Value("${api.key.param}")
    private String KEY_PARAM;

    @Value("${api.key.value}")
    private String KEY_VALUE;

    private final String PRODUCT_JSON_FILE = "/product_response.json";
    private final int TEST_PRODUCT_ID = 13860428;
    private final String TEST_PRODUCT_TITLE = "The Big Lebowski (Blu-ray)";

    @Autowired
    private MockRestServiceServer server;

    @Autowired
    private ProductCompositeService productCompositeService;

    @Test
    public void testGetOnlineDescription() throws URISyntaxException, IOException {
        UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder
                .fromHttpUrl(PRODUCT_COMPOSITE_HOST)
                .pathSegment(PRODUCTS_PATH)
                .pathSegment(PRODUCT_COMPOSITE_API_VERSION)
                .pathSegment(String.valueOf(TEST_PRODUCT_ID))
                .queryParam(FIELDS_PARAM, FIELDS_VALUE)
                .queryParam(ID_TYPE_PARAM, ID_TYPE_VALUE)
                .queryParam(KEY_PARAM, KEY_VALUE);
        String uriString = uriComponentsBuilder.build().encode().toUriString();
        URL url = this.getClass().getResource(PRODUCT_JSON_FILE);
        String productJson = String.join("\n", Files.readAllLines(Paths.get(url.toURI())));

        server.expect(min(1), requestTo(uriString)).andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess(productJson, MediaType.APPLICATION_JSON));
        String onlineDescription = productCompositeService.getOnlineDescription(TEST_PRODUCT_ID);
        assertThat(onlineDescription).isEqualTo(TEST_PRODUCT_TITLE);
        server.verify();
    }
}
