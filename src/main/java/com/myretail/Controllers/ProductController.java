package com.myretail.Controllers;

import com.myretail.Models.Product;
import com.myretail.Services.ProductService;
import org.springframework.web.bind.annotation.*;

/**
 * Created by michael.burris on 9/25/16.
 */
@RestController
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/products/{id}")
    public Product getProductById(@PathVariable int id) {
        return this.productService.findProductById(id);
    }

    @PutMapping("/products/{id}")
    public void putProductById(@PathVariable int id, @RequestBody Product product) {
        this.productService.updateProductCost(id, product.getProductCost().getValue());
    }
}
