package com.myretail.Services;

import com.myretail.Models.ProductCost;

/**
 * Created by michael.burris on 9/25/16.
 */
public interface ProductCostService {
    ProductCost getProductCost(int id);
    void updateProductCostValue(int id, double price);
}

