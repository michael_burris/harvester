package com.myretail.Services;

/**
 * Created by michael.burris on 9/25/16.
 */
public interface ProductCompositeService {
    String getOnlineDescription(int id);
}

