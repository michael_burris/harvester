package com.myretail.Services;

import com.myretail.Exceptions.ResourceNotFoundException;
import com.myretail.Models.Product;
import org.springframework.stereotype.Service;

/**
 * Created by michael.burris on 9/25/16.
 */
@Service
public class ProductServiceImpl implements ProductService {

    private final ProductCostService productCostService;
    private final ProductCompositeService productCompositeService;

    public ProductServiceImpl(ProductCostService productCostService, ProductCompositeService productCompositeService) {
        this.productCostService = productCostService;
        this.productCompositeService = productCompositeService;
    }

    private String getOnlineDescription(int id) {
        return this.productCompositeService.getOnlineDescription(id);
    }

    @Override
    public Product findProductById(int id) {
        if (this.getOnlineDescription(id) != null) {
            Product product = new Product();
            product.setProductId(id);
            product.setName(this.getOnlineDescription(id));
            product.setProductCost(this.productCostService.getProductCost(id));
            return product;
        } else {
            throw new ResourceNotFoundException();
        }
    }

    @Override
    public void updateProductCost(int id, double price) {
        this.productCostService.updateProductCostValue(id, price);
    }
}
