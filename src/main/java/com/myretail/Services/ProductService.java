package com.myretail.Services;

import com.myretail.Models.Product;

/**
 * Created by michael.burris on 9/25/16.
 */
public interface ProductService {
    Product findProductById(int id);
    void updateProductCost(int id, double price);
}

