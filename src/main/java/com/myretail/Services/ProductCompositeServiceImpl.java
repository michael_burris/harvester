package com.myretail.Services;

import com.myretail.Models.OnlineDescription;
import com.myretail.Models.ProductResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * Created by michael.burris on 9/25/16.
 */
@Service
public class ProductCompositeServiceImpl implements ProductCompositeService {

    @Value("${api.host}")
    private String PRODUCT_COMPOSITE_HOST;

    @Value("${api.products_path}")
    private String PRODUCTS_PATH;

    @Value("${api.version}")
    private String PRODUCT_COMPOSITE_API_VERSION;

    @Value("${api.fields.param}")
    private String FIELDS_PARAM;

    @Value("${api.fields.value}")
    private String FIELDS_VALUE;

    @Value("${api.id_type.param}")
    private String ID_TYPE_PARAM;

    @Value("${api.id_type.value}")
    private String ID_TYPE_VALUE;

    @Value("${api.key.param}")
    private String KEY_PARAM;

    @Value("${api.key.value}")
    private String KEY_VALUE;

    private final RestTemplate productCompositeRestTemplate;

    public ProductCompositeServiceImpl(RestTemplateBuilder restTemplateBuilder) {
        this.productCompositeRestTemplate = restTemplateBuilder.build();
    }

    @Override
    public String getOnlineDescription(int id) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

        UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder
                .fromHttpUrl(PRODUCT_COMPOSITE_HOST)
                .pathSegment(PRODUCTS_PATH)
                .pathSegment(PRODUCT_COMPOSITE_API_VERSION)
                .pathSegment(String.valueOf(id))
                .queryParam(FIELDS_PARAM, FIELDS_VALUE)
                .queryParam(ID_TYPE_PARAM, ID_TYPE_VALUE)
                .queryParam(KEY_PARAM, KEY_VALUE);
        String  uriString = uriComponentsBuilder.build().encode().toUriString();

        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        ResponseEntity<ProductResponse> responseEntity = this.productCompositeRestTemplate
                .exchange(uriString, HttpMethod.GET, httpEntity, ProductResponse.class);
        OnlineDescription onlineDescription = responseEntity.getBody().getProductCompositeResponse()
                .getItems().get(0).getOnlineDescription();
        if (onlineDescription != null) {
            return onlineDescription.getValue();
        } else {
            return null;
        }
    }
}
