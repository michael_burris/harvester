package com.myretail.Services;

import com.myretail.Models.ProductCost;
import com.myretail.Repositories.ProductCostRepository;
import org.springframework.stereotype.Service;

/**
 * Created by michael.burris on 9/25/16.
 */
@Service
public class ProductCostServiceImpl implements ProductCostService {

    private final ProductCostRepository productCostRepository;

    public ProductCostServiceImpl(ProductCostRepository productCostRepository) {
        this.productCostRepository = productCostRepository;
    }

    @Override
    public ProductCost getProductCost(int id) {
        return this.productCostRepository.findByProductId(id);
    }

    @Override
    public void updateProductCostValue(int id, double price) {
        ProductCost productCost = this.getProductCost(id);
        productCost.setValue(price);
        this.productCostRepository.save(productCost);
    }
}
