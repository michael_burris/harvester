package com.myretail.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by michael.burris on 9/25/16.
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Product not found")
public class ResourceNotFoundException extends RuntimeException {
}
