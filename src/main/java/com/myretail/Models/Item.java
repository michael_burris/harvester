package com.myretail.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by michael.burris on 9/25/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Item {

    @JsonProperty("online_description")
    private OnlineDescription onlineDescription;

    public Item() {
    }

    public OnlineDescription getOnlineDescription() {
        return onlineDescription;
    }

    public void setOnlineDescription(OnlineDescription onlineDescription) {
        this.onlineDescription = onlineDescription;
    }
}
