package com.myretail.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by michael.burris on 9/25/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductResponse {

    @JsonProperty("product_composite_response")
    private ProductCompositeResponse productCompositeResponse;

    public ProductResponse() {
    }

    public ProductCompositeResponse getProductCompositeResponse() {
        return productCompositeResponse;
    }

    public void setProductCompositeResponse(ProductCompositeResponse productCompositeResponse) {
        this.productCompositeResponse = productCompositeResponse;
    }
}
