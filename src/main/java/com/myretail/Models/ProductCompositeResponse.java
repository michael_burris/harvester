package com.myretail.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by michael.burris on 9/25/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductCompositeResponse {

    private List<Item> items;

    public ProductCompositeResponse() {
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}
