package com.myretail.Models;

/**
 * Created by michael.burris on 9/25/16.
 */
public class OnlineDescription {

    private String value;
    private String type;

    public OnlineDescription() {
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

