package com.myretail.Models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by michael.burris on 9/25/16.
 */
public class Product {

    @JsonProperty("id")
    private Integer productId;

    private String name;

    @JsonProperty("current_price")
    private ProductCost productCost;

    public Product() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public ProductCost getProductCost() {
        return productCost;
    }

    public void setProductCost(ProductCost productCost) {
        this.productCost = productCost;
    }
}
