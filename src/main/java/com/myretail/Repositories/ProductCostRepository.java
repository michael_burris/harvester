package com.myretail.Repositories;

import com.myretail.Models.ProductCost;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by michael.burris on 9/25/16.
 */
public interface ProductCostRepository extends MongoRepository<ProductCost, String> {
    ProductCost findByProductId(int productId);
}
