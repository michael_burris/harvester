# MyRetail Harvester

This is a Spring Boot project which receives
GET and PUT request at `/products/{id}`.  The built product is an executable jar
with embedded Tomcat server running on `localhost:8080`.

Product cost information will be pulled from a configured mongodb database, and 
combined with an external API request which is configured in `/src/main/resources/application.properties`

## Database Setup
Configure the application database in `/src/main/resources/application.properties`
Example: `spring.data.mongodb.uri=mongodb://localhost:27017/myretail`

For integration test `/src/test/java/com/myretail/HarvesterApplicationTests.java`
to pass a test database must be configured in `/src/main/resources/test.properties`
Example: `spring.data.mongodb.uri=mongodb://localhost:27017/testdb`

## External API Setup
In order for product information to be returned the external API must be configured.
Default settings should work in located in `/src/main/resources/application.properties`

```
api.host=https://api.target.com
api.products_path=products
api.version=v3
api.fields.param=fields
api.fields.value=descriptions
api.id_type.param=id_type
api.id_type.value=TCIN
api.key.param=key
api.key.value=43cJWpLjH8Z8oR18KdrZDBKAgLLQKJjz
```

## Build and Run
This app is built using gradle. Use the following commands to build and run the executable jar

```
$ gradle build
$ java -jar /libs/harvester-0.0.1-SNAPSHOT.jar
```

## Running Tests
To run the test `cd` into the project directory and run with gradle
`$ gradle test`

Optional setup the project in your IDE of choice.
